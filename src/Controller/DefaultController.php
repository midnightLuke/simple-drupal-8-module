<?php

namespace Drupal\lukes_module\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

class DefaultController extends ControllerBase {
  public function showPageAction() {
    // $client = \Drupal::service('lukes_module.trello_client');
    return [
      '#theme' => 'lukes_template',
      '#test' => 'Test',
    ];
  }

  public function nodeAction(Node $node) {
    return [
      '#theme' => 'lukes_node_template',
      '#node' => $node,
    ];
  }
}
