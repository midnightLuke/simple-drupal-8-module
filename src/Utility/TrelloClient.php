<?php

namespace Drupal\lukes_module\Utility;

use Drupal\lukes_module\Exception\TrelloClientException;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Subscriber\Oauth\Oauth1;
use Symfony\Component\HttpFoundation\Session\Session;

class TrelloClient extends Client {
  public function __construct($consumer_key, $consumer_secret, Session $session) {
    // dpm($consumer_key);
    // these are required to construct this
    if ($session->get('trello_token') === null
      || $session->get('trello_token_secret') === null) {

      // throw new TrelloClientException('Cannot create oauth handler, missing token or secret in session.');
    }

    // create oauth middleware
    // $middleware = new Oauth1([
    //   'consumer_key'    => $consumer_key,
    //   'consumer_secret' => $consumer_secret,
    //   'token'           => $session->get('trello_token'),
    //   'token_secret'    => $session->get('trello_token_secret'),
    // ]);

    // // create new stack and push on middleware
    // $stack = HandlerStack::create();
    // $stack->push($middleware);

    // create client with handler stack
    parent::__construct([
      'base_uri' => 'https://api.trello.com/1/',
      // 'handler' => $stack,
      // 'auth' => 'oauth',
    ]);
  }
}
